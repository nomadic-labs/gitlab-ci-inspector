#!/bin/sh

set -eu

require_command() {
  while [ $# -gt 1 ]; do
    if ! command -v "$1" > /dev/null 2>&1; then
      echo "Could not find $1"
      exit 1
    fi
    shift
  done
}

if [ -n "${GCI:-}" ]; then
  : # do nothing
elif command -v gitlab-ci-inspector > /dev/null 2>&1; then
  GCI=gitlab-ci-inspector
elif command -v gci > /dev/null 2>&1; then
  GCI=gci
else
  echo "Couldn't find GitLab CI Inspector in PATH. Either put it in PATH or set the environment variable GCI to point to the binary"
  exit 1
fi

if [ -n "${ICDIFF:-}" ]; then
  : # do nothing
elif command -v icdiff > /dev/null 2>&1; then
  ICDIFF="icdiff --whole-file --cols 150"
else
  ICDIFF="diff --color=always -y"
fi

if [ -n "${TRACE:-}" ]; then
  set -x
fi
