#!/bin/sh

SCRIPT_DIR="$(cd "$(dirname "$0")" && echo "$(pwd -P)/")"

# shellcheck source=./utils/gci.inc.sh
. "$SCRIPT_DIR"/gci.inc.sh

if [ "${1:-}" = "--help" ] || [ "${1:-}" = "--help" ]; then
  echo "Usage: $0 <dir>"
  echo
  echo "Lints the GitLab CI configuration in <dir> with all available environments. "
  exit 1
fi

if [ -n "${1:-}" ]; then
  dir=$1
else
  dir="."
fi

environments_file=$(mktemp)
$GCI show environments --format json > "$environments_file"
if ! jq empty < "$environments_file"; then
  echo "An invalid environment file was generated. This might indicate that your GitLab CI Inspector settings (currently, '$GCI') are invalid. Output: "
  cat "$environments_file"
  exit 1
fi
env_tot=$(jq -r '.environments|keys|length' < "$environments_file")
if [ "$env_tot" -eq 0 ]; then
  echo "The current settings define no environments: "
  cat "$environments_file"
  exit 1
fi

lint() {
  t=$(mktemp)
  $GCI --ci-file "$dir" lint -e "$environment" > "$t"
  cat "$t"
}

for environment in $(jq -r '.environments|keys[]' < "$environments_file"); do
  lint &
done

wait
