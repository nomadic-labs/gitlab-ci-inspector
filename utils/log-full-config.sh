#!/bin/sh

set -eu

SCRIPT_DIR="$(cd "$(dirname "$0")" && echo "$(pwd -P)/")"

# shellcheck source=./utils/gci.inc.sh
. "$SCRIPT_DIR"/gci.inc.sh

if [ "${1:-}" = "--help" ] || [ $# -lt 2 ]; then
  cat << EOT
Usage: $0 <revision-range> [<diff-full-config-params>]

Show the git commit logs of <revision-range>.

For each commit, also show the output of 'diff-full-config' for the
given commit. This diff can be parameterized by setting
<diff-full-config-params>. For more information, see 'diff-full-config
--help'.

EXAMPLES

  log-full-config master..HEAD all-envs

will show the message of each commit in the range master..HEAD,
accompanied with the diff of merged configuration for all
environments for that commit.
EOT
  exit 1
fi

range="$1"
shift

git log --format=%H "${range}" | while read -r commit; do
  git log --color=always -n1 "$commit"
  echo
  "$SCRIPT_DIR/diff-full-config.sh" "${commit}^" "${commit}" "$@" || true
  echo
done | if [ -n "${PAGER:-}" ]; then $PAGER; else cat; fi
