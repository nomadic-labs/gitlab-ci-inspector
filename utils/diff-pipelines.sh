#!/bin/sh

set -eu

SCRIPT_DIR="$(cd "$(dirname "$0")" && echo "$(pwd -P)/")"

# shellcheck source=./utils/gci.inc.sh
. "$SCRIPT_DIR"/gci.inc.sh

require_command jq

if [ "${1:-}" = "--help" ] || [ -z "${1:-}" ] || [ -z "${2:-}" ]; then
  echo "Usage: $0 <dir-before> <dif-after>"
  echo
  echo "Simulate a pipeline in all environments using two different configurations and diff the result per pipeline."
  exit 1
fi

before=$1
after=$2

ignore() { true; }

header() {
  printf "── Environment [\033[1m%s\033[0m]  " "${1}"

  range=$(seq 1 $((150 - 16 - ${#1} - 3)))
  for i in $range; do
    ignore "$i"
    printf "─"
  done
  printf "\n"
}

tmp=$(mktemp)

environments_file=$(mktemp)
$GCI show environments --format json > "$environments_file"
if ! jq empty < "$environments_file"; then
  echo "An invalid environment file was generated. This might indicate that your gci setting (currently, '$GCI') is invalid. Output: "
  cat "$environments_file"
  exit 1
fi
env_tot=$(jq -r '.environments|keys|length' < "$environments_file")
if [ "$env_tot" -eq 0 ]; then
  echo "The current settings define no environments: "
  cat "$environments_file"
  exit 1
fi

for environment in $(jq -r '.environments|keys[]' < "$environments_file"); do
  if ! "${SCRIPT_DIR}"/diff-pipeline.sh "$before" "$after" "$environment" > "$tmp"; then
    header "$environment"
    cat "$tmp"
  fi
done
