#!/bin/sh

set -eu

SCRIPT_DIR="$(cd "$(dirname "$0")" && echo "$(pwd -P)/")"

# shellcheck source=./utils/gci.inc.sh
. "$SCRIPT_DIR"/gci.inc.sh

usage() {
  cat << EOT
Usage: $0 [-e/--evaluation-strategy [evaluation-strategy|all-envs] ]
          [-d/--diff-only] [-D/--distinct-only]
          FIELD
          [COMPARE_WITH]?
          [-- GCI_OPTIONS]

Prints the value of the FIELD for each job in the .gitlab-ci.yml
configuration in the current directory. If COMPARE_WITH is set and
points to another directory containing an .gitlab-ci.yml
configuration, then the value of FIELD in the two configurations are
compared. In this case, [--diff-only] (or [-d] for short) limits
output to rows where the two configurations differ.

The [--evaluation-strategy] can be used to control how includes are
handled. For more information, see [gci merge --help] explanation
of this flag. This flag can take a comma-separated list of strategies
as defined by gci. The value of FIELD is then printed for each job
for each strategy. Finally, the special strategy 'all-envs' can be
used to print the value of FIELD for each job in each defined
environment. To limit output to environment-job pairs with distinct
fields, pass [--distinct-only] (or [-D] for short).

Under the hood, the "merge" command of gci is used to merge
.gitlab-ci.yml configurations. Extra options, can be appended to the
gci invocation after an [--] separator. See [gci merge --help]
for more information.

Examples:

  $0 tags

Prints the tags associated to each job in the configuration of the
current working directory.

  $0 artifacts.expire_in ../master

Compare the 'artifacts.expire_in' field in the current configuration
with that in the folder '../master'.

  $0 -e before_merging,master_branch needs

Prints the 'needs' configured to each job in the current configuration
in the 'before_merging' and 'master_branch' environments.

  $0 -e all-envs needs

Prints the 'needs' configured to each job in the current configuration
in all defined environments.
EOT
  exit 1
}

if [ "${1:-}" = "--help" ]; then
  usage
fi

require_command yq csvsql jq

failwith() {
  echo "$*"
  exit 1
}

yaml2json() {
  t=$(mktemp)
  cat > "$t"
  yq -o json . "$t"
  rm "$t"
}

# passed to gci's --evaluation-strategy.
# if set to 'all-envs', run for all environments
evaluation_strategy=always-include
diff_only=${DIFF_ONLY:-}
distinct_only=${DISTINCT_ONLY:-}

while [ $# -gt 0 ]; do
  if [ "$1" = "--" ]; then
    # Remaning $@ are GCI_OPTIONS
    shift
    break
  elif [ "$1" = "-e" ] || [ "$1" = "--evaluation-strategy" ]; then
    if [ -z "${2:-}" ]; then
      failwith "-e/--evaluation-strategy takes exactly one argument, see --help"
    fi
    evaluation_strategy="$2"
    shift
    shift
  elif [ "$1" = "-d" ] || [ "$1" = "--diff-only" ]; then
    diff_only="1"
    shift
  elif [ "$1" = "-D" ] || [ "$1" = "--distinct-only" ]; then
    distinct_only="1"
    shift
  elif [ -z "${field+x}" ]; then
    field="$1"
    shift
  elif [ -z "${compare_with+x}" ]; then
    compare_with="$1"
    shift
  else
    usage
  fi
done

if [ -z "${field:-}" ]; then
  failwith "field is not set, see --help"
fi

if { [ -n "${compare_with:-}" ] && ! [ -d "$compare_with" ]; }; then
  failwith "'$compare_with' is not an existing directory, see --help"
fi

environments() {
  environments_file=$(mktemp)
  $GCI show environments --format json > "$environments_file"
  if ! jq empty < "$environments_file"; then
    echo "An invalid environment file was generated. This might indicate that your gci setting (currently, '$GCI') is invalid. Output: "
    cat "$environments_file"
    exit 1
  fi
  jq -r '.environments|keys[]' < "$environments_file"
}

csv_header() {
  jq -n -cr "[\"Env\", \"Stage\", \"Job name\", \"field $field\"]|@csv"
}

field_per_job_csv() {
  evaluation_strategy="$1"
  shift

  $GCI -s plain merge -t expand -e "${evaluation_strategy}" "$@" |
    yaml2json |
    jq -cr "(to_entries
                  | map(select(.value | type == \"object\"))
                  | map(select(.key as \$key | [\"image\", \"stages\", \"variables\", \"include\", \"workflow\", \"default\"] | index(\$key) | not))
                  | map(select(.key | startswith(\".\") | not))
                  | map([\"$evaluation_strategy\", .value.stage, .key, (.value.$field | tostring)]))[] | @csv"
}

strategies() {
  if [ "$evaluation_strategy" = "all-envs" ]; then
    environments
  else
    echo "$evaluation_strategy" | tr ',' "\t"
  fi
}

field_per_job_csv_per_env() {
  (
    dir="$1"
    shift
    cd "$dir" || exit 1

    csv_header
    for strategy in $(strategies); do
      field_per_job_csv "$strategy" "$@"
    done
  )
}

maybe_distinct_only() {
  if [ "${distinct_only}" = "1" ]; then
    csvsql --table jobs --query "SELECT * FROM jobs GROUP BY jobs.'Job name', jobs.${field} ORDER BY Env, Stage"
  else
    cat
  fi
}

if [ -z "${compare_with:-}" ]; then
  field_per_job_csv_per_env "." "$@" | maybe_distinct_only
else
  before=$(mktemp)
  after=$(mktemp)

  field_per_job_csv_per_env "$compare_with" "$@" > "$before"
  field_per_job_csv_per_env "." "$@" > "$after"

  if [ "${diff_only:-}" = 1 ]; then
    where=" WHERE before.'field $field' <> after.'field $field'"
  else
    where=""
  fi

  # --no-inference: avoid converting strings like '90 days' to timestamps
  # --blanks: avoid converting strings like '' to NULL
  csvsql --no-inference \
    --blanks \
    "$before" "$after" \
    --tables before,after \
    --query "
SELECT
    before.'Env',
    before.'Stage',
    before.'Job name',
    before.'field $field' as \"$field ($compare_with)\",
    after.'field $field' as \"$field\"
FROM before
LEFT JOIN after ON (after.'Job name' = before.'Job name' AND after.'Env' = before.'Env')
${where}
"

  rm -f "$before" "$after"
fi
