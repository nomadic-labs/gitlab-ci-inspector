open Stuff.Base

type workflow_when_val = Always | Never

let workflow_when_val_to_string = function
  | Always -> "always"
  | Never -> "never"

type when_val = On_success | On_failure | Always | Manual | Delayed | Never

(* todo: move to semantics *)
let job_default_when : when_val = On_success

let when_val_to_string = function
  | On_success -> "on_success"
  | On_failure -> "on_failure"
  | Always -> "always"
  | Manual -> "manual"
  | Delayed -> "delayed"
  | Never -> "never"

let pp_when_val fmt w = Format.fprintf fmt "%s" (when_val_to_string w)

type changes = {
  (* TODO: investigate the semantics of an empty paths list *)
  paths : Glob.glob list;
  compare_to : string option;
}

type rule = {
  if_ : Expr_AST.t option;
  changes : changes option;
  (* exists : string list; *)
  when_ : when_val option;
  (* start_in:  *)
  allow_failure : bool option;
  variables : Env.t;
  (* meta fields *)
  source__ : Yaml.value option;
  index__ : int option;
}

let default_rule : rule =
  {
    if_ = None;
    changes = None;
    when_ = None;
    allow_failure = None;
    variables = Env.empty ();
    index__ = None;
    source__ = None;
  }

let pp_rule fmt {source__; index__; _} =
  match source__ with
  | None -> Format.fprintf fmt "<rule, source unknown>"
  | Some source -> (
      match Yaml.to_string ~layout_style:`Flow source with
      | Ok s ->
          Format.fprintf
            fmt
            "%a:%s"
            (Format.pp_print_option
               ~none:(fun fmt () -> Format.pp_print_string fmt "?")
               Format.pp_print_int)
            index__
            (String.trim s)
      | Error (`Msg m) ->
          Format.pp_print_string fmt (Printf.sprintf "(error (%s))" m))

let rec eval_rules :
    ?job_name:string ->
    changed_files:String_set.t ->
    Env.t ->
    rule list ->
    rule option =
 fun ?job_name ~changed_files env rs ->
  let open Stuff in
  match rs with
  | ({if_; changes; _} as rule) :: rs ->
      let if_value =
        match if_ with
        | None ->
            (* if the job has no if clause, then it defaults to true *)
            Log.debug
              "[if:] clause of rule %a vacuously evaluated to true (missing \
               `if`)\n"
              pp_rule
              rule ;
            true
        | Some expr -> (
            match Expr_eval.eval_bool env expr with
            | Some true ->
                Log.debug
                  "[if:] clause of rule %a evaluated to true (expression: %a, \
                   variables: %a)\n"
                  pp_rule
                  rule
                  Expr_AST.pp
                  expr
                  Env.pp
                  env ;
                true
            | Some false ->
                Log.debug
                  "[if:] clause of rule %a evaluated to false (expression: %a, \
                   variables: %a)\n"
                  pp_rule
                  rule
                  Expr_AST.pp
                  expr
                  Env.pp
                  env ;
                false
            | None -> assert false)
      in
      let changes_value =
        match changes with
        | Some {paths; compare_to} ->
            let source_opt = Hashtbl.find_opt env "CI_PIPELINE_SOURCE" in
            let commit_branch_opt = Hashtbl.find_opt env "CI_COMMIT_BRANCH" in
            let vacuous_changes =
              match (source_opt, commit_branch_opt, compare_to) with
              | Some "merge_request_event", _, _ -> false
              | Some "push", Some _, _ -> false
              | _, _, Some _ -> false
              | _ ->
                  Log.warn
                    "The [changes:] clause in rule %a%s vacuously evaluates to \
                     true in this environment. See the README.md for more \
                     info."
                    pp_rule
                    rule
                    (match job_name with
                    | Some job_name -> Printf.sprintf " of job '%s'" job_name
                    | None -> "") ;
                  true
            in
            if vacuous_changes then true
            else
              String_set.exists
                (fun changed_file ->
                  List.exists
                    (fun path_pattern ->
                      let glob_match = Glob.(changed_file =~ path_pattern) in
                      if glob_match then
                        Log.debug
                          "path pattern %S in [changes:] clause of rule %a \
                           matched changed file %S\n"
                          (Glob.show path_pattern)
                          pp_rule
                          rule
                          changed_file
                      else
                        Log.debug
                          "path pattern %S in [changes:] clause of rule %a did \
                           not match changed file %S\n"
                          (Glob.show path_pattern)
                          pp_rule
                          rule
                          changed_file ;
                      glob_match)
                    paths)
                changed_files
        | _ -> true
      in
      (* The evaluation of the rule is the conjunction of the [if:] and the [changes:] *)
      if if_value && changes_value then Some rule
      else eval_rules ?job_name ~changed_files env rs
  | [] ->
      Log.debug "No rules matched\n" ;
      None

(* a workflow is either absent or has a non-empty set of rules *)
type workflow = {rules : rule list}

let pp_workflow fmt {rules} =
  Format.fprintf fmt "workflow: \n" ;
  List.iter (pp_rule fmt) rules

type job_allow_failure =
  | Never_allow
  | Always_allow
  | Allow_on_exit_codes of int list
[@@deriving show {with_path = false}]

type job = {
  stage : string;
  extends : string list;
  dependencies : string list option;
  before_script : string list;
  script : string list;
  after_script : string list;
  coverage : string option;
  allow_failure : job_allow_failure option;
  (* no rules and an empty rule list is not the same: *)
  (* if a job has no rules field  then it is always included. *)
  (* if a job has a rules field which is the empty list, then it is never included *)
  rules : rule list option;
  variables : Env.t;
  when_ : when_val option;
}
[@@deriving show {with_path = false}]

(* type stage = job list *)

(* type pipeline = {variables : variable list; stages : stage list} *)
