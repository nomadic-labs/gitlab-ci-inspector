(* TODO: do not use a mutable structure *)

type t = (string, string) Hashtbl.t

let of_list l = Hashtbl.of_seq (List.to_seq l)

let to_list e = List.of_seq (Hashtbl.to_seq e)

let is_empty env = (Hashtbl.to_seq env) () = Seq.Nil

let empty () = Hashtbl.create 5

let pp fmt env =
  Format.(
    fprintf
      fmt
      "{%a}"
      (pp_print_list
         ~pp_sep:(fun fmt () -> fprintf fmt ", ")
         (fun fmt (x, v) -> fprintf fmt {|"%s": "%s"|} x v))
      (to_list env))

let merge env_left env_right = Hashtbl.iter (Hashtbl.replace env_left) env_right
