(* https://docs.gitlab.com/ee/ci/jobs/job_control.html#only--except-regex-syntax *)

(* https://github.com/google/re2/wiki/Syntax *)

(* IIUC, Re2 supports BOTH Perl and Emacs ish syntax, not sure how to deal with this with Re! *)

type opt = [`Caseless | `Dotall | `Multiline | `Ungreedy]

type t = string * Re.re * opt list

let opt_opt : opt -> Re.Perl.opt = function
  | `Caseless -> `Caseless
  | `Dotall -> `Dotall
  | `Multiline -> `Multiline
  | `Ungreedy -> `Ungreedy

let show_opt = function
  | `Caseless -> "i"
  | `Dotall -> "s"
  | `Multiline -> "m"
  | `Ungreedy -> "U"

let compile ?(opts = []) r =
  ( r,
    (let opts = List.map opt_opt opts in
     Re.compile (Re.Perl.re ~opts r)),
    opts )

let literal s = (s, Re.compile (Re.str s), [])

let compile_flags ~flags r =
  let opts =
    String.fold_right
      (fun c flags ->
        (* https://github.com/google/re2/wiki/Syntax#perl *)
        match c with
        | 'i' -> `Caseless :: flags
        | 's' -> `Dotall :: flags
        | 'm' -> `Multiline :: flags
        | 'U' -> `Ungreedy :: flags
        | _ -> failwith (Printf.sprintf "[Re2.compile] Unsupported flag '%c'" c))
      flags
      []
  in
  compile ~opts r

let pp : Format.formatter -> t -> unit =
 fun fmt (r, _, opts) ->
  Format.fprintf fmt "/%s/%s" r (String.concat "" (List.map show_opt opts))

let ( =~ ) s (_, r, _) = Re.execp r s

let ( =~! ) s (_, r, _) = not (Re.execp r s)

let get_group group index =
  match Re.Group.get group index with
  | exception Not_found ->
      invalid_arg
        "regular expression has not enough capture groups for its usage, did \
         you forget parentheses?"
  | value -> value

let ( =~* ) s (_, r, _) =
  match Re.exec_opt r s with
  | None -> None
  | Some group -> Some (get_group group 1)

let ( =~** ) s (_, r, _) =
  match Re.exec_opt r s with
  | None -> None
  | Some group -> Some (get_group group 1, get_group group 2)

let ( =~*** ) s (_, r, _) =
  match Re.exec_opt r s with
  | None -> None
  | Some group -> Some (get_group group 1, get_group group 2, get_group group 3)

let ( =~^ ) s (_, r, _) =
  match Re.exec_opt r s with
  | None -> None
  | Some group -> Some (Re.Group.all group)

let%expect_test "test re2" =
  let ( =~ ) s re = Format.printf "%b" (s =~ compile re) in
  (* basics *)
  "a regexp" =~ "a regexp" ;
  [%expect {| true |}] ;
  "foobarbaz" =~ "bar" ;
  [%expect {| true |}] ;
  "baz" =~ "bar" ;
  [%expect {| false |}] ;

  (* version numbers *)
  let vre = {|\A\d+\.\d+\.\d+\z|} in
  "1.2.3" =~ vre ;
  [%expect {| true |}] ;
  "1.foo.3" =~ vre ;
  [%expect {| false |}] ;
  " 1.2.3" =~ vre ;
  [%expect {| false |}] ;
  "1.2.3 " =~ vre ;
  [%expect {| false |}] ;

  (* labels *)
  let arm64_lbl = {|(?:^|[,])ci--arm64(?:$|[,])|} in
  "ci--arm64" =~ arm64_lbl ;
  [%expect {| true |}] ;
  "foobar,ci--arm64" =~ arm64_lbl ;
  [%expect {| true |}] ;
  "ci--arm64,barfoo" =~ arm64_lbl ;
  [%expect {| true |}] ;
  "ci,ci--arm64,barfoo" =~ arm64_lbl ;
  [%expect {| true |}] ;
  "ci,ci--arm64,barfoo" =~ arm64_lbl ;
  [%expect {| true |}] ;
  "foo,bar,baz" =~ arm64_lbl ;
  [%expect {| false |}] ;
  "" =~ arm64_lbl ;
  [%expect {| false |}] ;

  (* releases *)
  let rel = {|-release$|} in
  "v1-release" =~ rel ;
  [%expect {| true |}] ;
  "v1-release-test" =~ rel ;
  [%expect {| false |}] ;
  "release-test" =~ rel ;
  [%expect {| false |}]
