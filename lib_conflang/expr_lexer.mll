(*****************************************************************************)
(*                                                                           *)
(* SPDX-License-Identifier: MIT                                              *)
(* Copyright (c) 2023 Nomadic Labs <contact@nomadic-labs.com>                *)
(*                                                                           *)
(*****************************************************************************)

{
  open Expr_parser

  exception Error of string

  let error x = Printf.ksprintf (fun e -> raise (Error e)) x
}

(* See {{:https://docs.gitlab.com/ee/ci/yaml/index.html#variables} here}
   for valid variable names. *)
let variable_word = ['a'-'z' 'A'-'Z' '0'-'9' '_']*
let regexp_sym_char = ['a'-'z' 'A'-'Z' '+' '*' '.' '/' '-']
let regexp_flag_char = ['i' 's' 'm' 'U']
let regexp_flags = ['i' 's' 'm' 'U']*

rule token = parse
  | ' '+ { token lexbuf }
  | '"' { STRING (double_quoted_string (Buffer.create 128) lexbuf) }
  | '\'' { STRING (single_quoted_string (Buffer.create 128) lexbuf) }
  | '/' { RE (re (Buffer.create 128) lexbuf) }
  | '$' (variable_word as x) { VAR x }
  | "null" { NULL }
  | "==" { EQUAL }
  | "!=" { NOT_EQUAL }
  | "=~" { MATCHES }
  | "!~" { NOT_MATCHES }
  | '(' { LPAR }
  | ')' { RPAR }
  | "||" { OR }
  | "&&" { AND }
  | _ as c { error "parse error near: %C" c }
  | eof { EOF }

and double_quoted_string buffer = parse
  | '"' { Buffer.contents buffer }
  | [^'"']+ as x { Buffer.add_string buffer x; double_quoted_string buffer lexbuf }
  | eof { error "unterminated double-quoted string" }

and single_quoted_string buffer = parse
  | '\'' { Buffer.contents buffer }
  | [^'\'']+ as x { Buffer.add_string buffer x; single_quoted_string buffer lexbuf }
  | eof { error "unterminated single-quoted string" }


and re buffer = parse
  | '/' (regexp_flags as f) { (Buffer.contents buffer, f) }
  | "\\\\" { Buffer.add_char buffer '\\'; re buffer lexbuf }
  | "\\" (regexp_sym_char as c) { Buffer.add_char buffer '\\'; Buffer.add_char buffer c; re buffer lexbuf }
  | [^'/' '\\']+ as x { Buffer.add_string buffer x; re buffer lexbuf }
  | eof { error "unterminated regular expression" }

