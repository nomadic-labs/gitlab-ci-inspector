(*****************************************************************************)
(*                                                                           *)
(* SPDX-License-Identifier: MIT                                              *)
(* Copyright (c) 2023 Nomadic Labs. <contact@nomadic-labs.com>               *)
(*                                                                           *)
(*****************************************************************************)

(** Evaluation of GitLab CI expressions.

    GitLab CI expressions are evaluated in an {!Env} that consists of
    a mapping of variable names to string values. The result of
    evaluation is a {!value}. Evaluation is partial: the result of
    comparisons whose left-hand side evaluates to a regexp and whose
    right-hand side evaluates to a string or a boolean is not
    defined. In GitLab CI, this results in a run-time error -- here we
    return an optional value. *)

(** The value of an evaluation. *)
type value = S of string | R of Re2.t | B of bool | Null

(** Pretty-prints a {!value} *)
val pp_value : Format.formatter -> value -> unit

(** Evaluates a GitLab CI expression.

    [eval env exp] returns [Some v] where [v] is the evaluation
    of [exp] in the environment [env]. If the expression is invalid,
    [None] is returned. *)
val eval : Env.t -> Expr_AST.t -> value option

(** Evaluates a GitLab CI expression to a boolean.

    [eval_bool env exp] returns [Some b] where [b] is the boolean
    interpretation of [eval env exp]. If the expression is invalid,
    [None] is returned.

    The boolean interpretation of a value is as follows:

    - The empty string value ([S ""]) is false
    - Non-empty string values ([S _]) and all regexp values ([R _]) are [true].
    - {!Null} is [false].
    - Boolean values [B b] are [b]. *)
val eval_bool : Env.t -> Expr_AST.t -> bool option
