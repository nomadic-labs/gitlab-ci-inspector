(*****************************************************************************)
(*                                                                           *)
(* SPDX-License-Identifier: MIT                                              *)
(* Copyright (c) 2023 Nomadic Labs. <contact@nomadic-labs.com>               *)
(*                                                                           *)
(*****************************************************************************)

open Tezt
open Tezt.Base

module TSV = struct
  type 'a parse_cell = line:int -> col:int -> string -> 'a

  and ('k, 'res) parse_row =
    | [] : ('res, 'res) parse_row
    | ( :: ) :
        'a parse_cell * ('k, 'res) parse_row
        -> ('a -> 'k, 'res) parse_row

  let string : string parse_cell = fun ~line:_ ~col:_ s -> s

  let bool : bool parse_cell =
   fun ~line ~col s ->
    match s with
    | "true" -> true
    | "false" -> false
    | _ ->
        Test.fail
          "[TSV.bool] expected string true or false on line %d, column %d"
          line
          col

  let apply : line:int -> string list -> ('a, 'b) parse_row -> 'a -> 'b =
   fun ~line cells parsers ->
    let parsers_len =
      let rec count : type k res. (k, res) parse_row -> int = function
        | [] -> 0
        | _ :: ps -> 1 + count ps
      in
      count parsers
    in
    let rec apply2_aux :
        type k res. col:int -> string list -> (k, res) parse_row -> k -> res =
     fun ~col ss parse ->
      match (ss, parse) with
      | [], [] -> fun x -> x
      | s :: ss', p :: ps ->
          fun f ->
            let v = p ~line ~col s in
            apply2_aux ~col:(col + 1) ss' ps (f v)
      | _ :: _, [] | [], _ :: _ ->
          Test.fail
            "[apply] expected exactly %d columns on line %d, found %d"
            parsers_len
            line
            (List.length cells)
    in
    apply2_aux ~col:0 cells parsers

  let read_tsv : ?has_header:bool -> string -> string list list =
   fun ?(has_header = true) file ->
    let rows =
      Base.read_file file |> String.split_on_char '\n'
      |> List.map (String.split_on_char '\t')
    in
    let ncols, (rows : string list list) =
      match rows with
      | [] -> (0, [])
      | row :: rows' -> (List.length row, if has_header then rows' else rows)
    in
    (* Sanity check the length of each row *)
    let rows =
      List.mapi
        (fun i row ->
          let ncols' = List.length row in
          let line = i + 1 + if has_header then 1 else 0 in
          match row with
          | [""] -> (* ignore empty rows, e.g. last line *) None
          | _ ->
              if ncols' <> ncols then
                Test.fail
                  "Row on line %d (%S) has %d column(s), expected %d"
                  line
                  (String.concat "\t" row)
                  ncols'
                  ncols
              else Some row)
        rows
    in
    List.filter_map Fun.id rows

  let iteri :
      type k.
      ?has_header:bool ->
      string ->
      (k, unit) parse_row ->
      (line:int -> k) ->
      unit =
   fun ?(has_header = true) file parse f ->
    List.iteri
      (fun i cells ->
        let line = i + 1 + if has_header then 1 else 0 in
        apply ~line cells parse (f ~line))
      (read_tsv ~has_header file)

  let iteri_p :
      type k.
      ?has_header:bool ->
      string ->
      (k, unit Lwt.t) parse_row ->
      (line:int -> k) ->
      unit Lwt.t =
   fun ?(has_header = true) file parse f ->
    Lwt_list.iteri_p
      (fun i cells ->
        let line = i + 1 + if has_header then 1 else 0 in
        apply ~line cells parse (f ~line))
      (read_tsv ~has_header file)
end

let () =
  Test.register ~__FILE__ ~title:"test_globs.tsv" ~tags:["conflang"; "glob"]
  @@ fun () ->
  return
  @@ TSV.(iteri "test_globs.tsv" [string; string; bool])
  @@ fun ~line pattern file should_match ->
  let match_ = Conflang.Glob.(file =~ glob pattern) in
  let error_msg =
    sf
      "Row %d: Expected that %S =~ %S ==> %b, got %b"
      (line + 2)
      file
      pattern
      should_match
      match_
  in
  Check.((match_ = should_match) bool ~__LOC__ ~error_msg)

let () =
  Test.register
    ~__FILE__
    ~title:"test_globs.tsv coherence"
    ~tags:["conflang"; "glob"]
  @@ fun () ->
  let ruby_fnmatch ~file ~pattern =
    let ruby_program =
      {|
          flags = (File::FNM_PATHNAME | File::FNM_DOTMATCH | File::FNM_EXTGLOB)
          puts File.fnmatch(ARGV[0], ARGV[1], flags)
          |}
    in
    let* output =
      Process.run_and_read_stdout
        "ruby"
        ["-e"; ruby_program; "--"; pattern; file]
    in
    match String.trim output with
    | "true" -> return true
    | "false" -> return false
    | _ -> Test.fail "[ruby_fnmatch] Unexpected output from ruby: %S" output
  in
  let* has_ruby =
    let p = Process.spawn "which" ["ruby"] in
    let* status = Process.wait p in
    return (match status with Unix.WEXITED 0 -> true | _ -> false)
  in
  if has_ruby then (
    TSV.(iteri_p "test_globs.tsv" [string; string; bool])
    @@ fun ~line pattern file should_match ->
    let* ruby_match = ruby_fnmatch ~file ~pattern in
    let error_msg =
      sf
        "as per test_globs.tsv:%d, %S =~ %S ==> %b, however, ruby says %b"
        line
        file
        pattern
        should_match
        ruby_match
    in
    Check.((ruby_match = should_match) bool ~__LOC__ ~error_msg) ;
    unit)
  else (
    Log.warn "Ruby interpreter is not installed, test is skipped." ;
    unit)

let () =
  Test.register ~__FILE__ ~title:"test_char_class" ~tags:["conflang"; "glob"]
  @@ fun () ->
  let string_of_char c = String.make 1 c in
  let ( =~ ) s g =
    let r = Conflang.Glob.(s =~ glob g) in
    Log.debug "%S =~ %S ==> %b" s g r ;
    r
  in
  let ( =~! ) s g =
    let r = Conflang.Glob.(s =~! glob g) in
    Log.debug "%S =~! %S ==> %b" s g r ;
    r
  in
  let bracket_test s t =
    for i = 0x21 to 0x7E do
      let c = Char.chr i in
      (* Forward slash is a special case when pathname is set *)
      if c <> '/' then (
        let sc = string_of_char c in
        Log.debug "testing %C" c ;
        let inc = String.contains t c in
        (* inclusion *)
        let b0 = sc =~ "[" ^ s ^ "]" in
        let error_msg = sf "expected that %s =~ [%s] ==> %b" sc s inc in
        Check.((inc = b0) bool ~__LOC__ ~error_msg) ;
        (* exclusion with '^' *)
        let b1 = sc =~! "[^" ^ s ^ "]" in
        let error_msg = sf "expected that %s =~! [^%s] ==> %b" sc s inc in
        Check.((inc = b1) bool ~__LOC__ ~error_msg) ;
        (* exclusion with '!' *)
        let b2 = sc =~! "[!" ^ s ^ "]" in
        let error_msg = sf "expected that %s =~! [!%s] ==> %b" sc s inc in
        Check.((inc = b2) bool ~__LOC__ ~error_msg) ;
        Log.debug "%d (%c, inc = %b): %b, %b, %b" i c inc b0 b1 b2)
    done
  in
  bracket_test "bd-gikl-mosv-x" "bdefgiklmosvwx" ;
  unit

let () = Test.run ()
