(*****************************************************************************)
(*                                                                           *)
(* SPDX-License-Identifier: MIT                                              *)
(* Copyright (c) 2023 Nomadic Labs. <contact@nomadic-labs.com>               *)
(*                                                                           *)
(*****************************************************************************)

open Conflang
open Conflang.Expr_eval

let pp res =
  Format.printf
    "%a"
    (Format.pp_print_option
       ~none:(fun fmt () -> Format.fprintf fmt "None")
       pp_value)
    res

let test_eval ?(env = Hashtbl.create 5) e = pp (eval env e)

let test_eval_bool ?(env = Hashtbl.create 5) e =
  let pp res =
    Format.printf
      "%a"
      Format.(
        pp_print_option ~none:(fun fmt () -> fprintf fmt "None") pp_print_bool)
      res
  in
  pp (eval_bool env e)

let%expect_test "test expression evaluation" =
  test_eval (Cmp (Str "foo", Eq, Str "foo")) ;
  [%expect {| (B true) |}] ;
  test_eval (Cmp (Str "foo", Neq, Str "foo")) ;
  [%expect {| (B false) |}] ;
  test_eval (Cmp (Str "foo", Eq, Str "bar")) ;
  [%expect {| (B false) |}] ;
  test_eval (Cmp (Str "foo", Neq, Str "bar")) ;
  [%expect {| (B true) |}] ;

  (* Vars *)
  test_eval_bool ~env:(Env.of_list [("foo", "bar")]) (Var "foo") ;
  [%expect {| true |}] ;

  test_eval_bool (Var "foo") ;
  [%expect {| false |}] ;

  test_eval_bool
    ~env:(Env.of_list [("foo", "bar")])
    (Cmp (Var "foo", Eq, Str "bar")) ;
  [%expect {| true |}] ;

  test_eval_bool
    ~env:(Env.of_list [("foo", "bar")])
    (Cmp (Var "foo", Eq, Str "foo")) ;
  [%expect {| false |}] ;

  test_eval_bool
    ~env:(Env.of_list [("FOO", "foo")])
    (Cmp (Var "FOO", Eq, Str "foo")) ;
  [%expect {| true |}]

(** Tests borrowed from
    {{:https://gitlab.com/gitlab-org/gitlab/blob/master/spec/lib/gitlab/ci/pipeline/expression/statement_spec.rb}spec/lib/gitlab/ci/pipeline/expression/statement_spec.rb} *)

let%expect_test "test expression evaluation: statement_spec.rb" =
  let env =
    Env.of_list
      [
        ("PRESENT_VARIABLE", "my variable");
        ("PATH_VARIABLE", "a/path/variable/value");
        ("FULL_PATH_VARIABLE", "/a/full/path/variable/value");
        ("EMPTY_VARIABLE", "");
      ]
  in
  let test_eval exp =
    match Expr.parse_opt exp with
    | None -> print_endline "/!\\ could not parse /!\\"
    | Some exp -> test_eval ~env exp
  in
  let test_eval_bool exp =
    match Expr.parse_opt exp with
    | None -> print_endline "/!\\ could not parse /!\\"
    | Some exp -> test_eval_bool ~env exp
  in
  test_eval {|$PRESENT_VARIABLE == "my variable"|} ;
  [%expect {| (B true) |}] ;
  test_eval {|"my variable" == $PRESENT_VARIABLE|} ;
  [%expect {| (B true) |}] ;
  test_eval {|$PRESENT_VARIABLE == null|} ;
  [%expect {| (B false) |}] ;
  test_eval {|$EMPTY_VARIABLE == null|} ;
  [%expect {| (B false) |}] ;
  test_eval {|"" == $EMPTY_VARIABLE|} ;
  [%expect {| (B true) |}] ;
  test_eval {|$EMPTY_VARIABLE|} ;
  [%expect {| (S "") |}] ;
  test_eval {|$UNDEFINED_VARIABLE == null|} ;
  [%expect {| (B true) |}] ;
  test_eval {|null == $UNDEFINED_VARIABLE|} ;
  [%expect {| (B true) |}] ;
  test_eval {|$PRESENT_VARIABLE|} ;
  [%expect {| (S "my variable") |}] ;
  test_eval {|$UNDEFINED_VARIABLE|} ;
  [%expect {| Null |}] ;
  test_eval {|$PRESENT_VARIABLE =~ /var.*e$/|} ;
  [%expect {| (B true) |}] ;
  test_eval {|$PRESENT_VARIABLE =~ /va\r.*e$/|} ;
  [%expect {| (B false) |}] ;
  test_eval {|$PRESENT_VARIABLE =~ /va\/r.*e$/|} ;
  [%expect {| (B false) |}] ;
  test_eval {|$PRESENT_VARIABLE =~ /var.*e$/|} ;
  [%expect {| (B true) |}] ;
  test_eval {|$PRESENT_VARIABLE =~ /^var.*/|} ;
  [%expect {| (B false) |}] ;
  test_eval {|$EMPTY_VARIABLE =~ /var.*/|} ;
  [%expect {| (B false) |}] ;
  test_eval {|$UNDEFINED_VARIABLE =~ /var.*/|} ;
  [%expect {| (B false) |}] ;
  test_eval {|$PRESENT_VARIABLE =~ /VAR.*/i|} ;
  [%expect {| (B true) |}] ;
  test_eval {|$PATH_VARIABLE =~ /path\/variable/|} ;
  [%expect {| (B true) |}] ;
  test_eval {|$FULL_PATH_VARIABLE =~ /^\/a\/full\/path\/variable\/value$/|} ;
  [%expect {| (B true) |}] ;
  test_eval {|$FULL_PATH_VARIABLE =~ /\/path\/variable\/value$/|} ;
  [%expect {| (B true) |}] ;
  test_eval {|$PRESENT_VARIABLE != "my variable"|} ;
  [%expect {| (B false) |}] ;
  test_eval {|"my variable" != $PRESENT_VARIABLE|} ;
  [%expect {| (B false) |}] ;
  test_eval {|$PRESENT_VARIABLE != null|} ;
  [%expect {| (B true) |}] ;
  test_eval {|$EMPTY_VARIABLE != null|} ;
  [%expect {| (B true) |}] ;
  test_eval {|"" != $EMPTY_VARIABLE|} ;
  [%expect {| (B false) |}] ;
  test_eval {|$UNDEFINED_VARIABLE != null|} ;
  [%expect {| (B false) |}] ;
  test_eval {|null != $UNDEFINED_VARIABLE|} ;
  [%expect {| (B false) |}] ;
  test_eval {|$PRESENT_VARIABLE !~ /var.*e$/|} ;
  [%expect {| (B false) |}] ;
  test_eval {|$PRESENT_VARIABLE !~ /^var.*/|} ;
  [%expect {| (B true) |}] ;
  (* TODO: support \a (bell) in Re2 *)
  (* test_eval {|$PRESENT_VARIABLE !~ /^v\ar.*/|} ; *)
  (* [%expect {| (B true) |}] ; *)
  (* test_eval {|$PRESENT_VARIABLE !~ /^v\/ar.*/|} ; *)
  (* [%expect {| (B true) |}] ; *)
  test_eval {|$EMPTY_VARIABLE !~ /var.*/|} ;
  [%expect {| (B true) |}] ;
  test_eval {|$UNDEFINED_VARIABLE !~ /var.*/|} ;
  [%expect {| (B true) |}] ;
  test_eval {|$PRESENT_VARIABLE !~ /VAR.*/i|} ;
  [%expect {| (B false) |}] ;
  (* More *)
  test_eval {|$PRESENT_VARIABLE && "string"|} ;
  [%expect {|(S "string")|}] ;
  test_eval {|$PRESENT_VARIABLE && $PRESENT_VARIABLE|} ;
  [%expect {|(S "my variable")|}] ;
  test_eval {|$PRESENT_VARIABLE && $EMPTY_VARIABLE|} ;
  [%expect {|(S "")|}] ;
  test_eval {|$PRESENT_VARIABLE && null|} ;
  [%expect {|Null|}] ;
  test_eval {|"string" && $PRESENT_VARIABLE|} ;
  [%expect {|(S "my variable")|}] ;
  test_eval {|$EMPTY_VARIABLE && $PRESENT_VARIABLE|} ;
  [%expect {|(S "my variable")|}] ;
  test_eval {|null && $PRESENT_VARIABLE|} ;
  [%expect {|Null|}] ;
  test_eval {|$EMPTY_VARIABLE && "string"|} ;
  [%expect {|(S "string")|}] ;
  test_eval {|$EMPTY_VARIABLE && $EMPTY_VARIABLE|} ;
  [%expect {|(S "")|}] ;
  test_eval {|"string" && $EMPTY_VARIABLE|} ;
  [%expect {|(S "")|}] ;
  test_eval {|"string" && null|} ;
  [%expect {|Null|}] ;
  test_eval {|null && "string"|} ;
  [%expect {|Null|}] ;
  test_eval {|"string" && "string"|} ;
  [%expect {|(S "string")|}] ;
  test_eval {|null && null|} ;
  [%expect {|Null|}] ;
  test_eval {|$PRESENT_VARIABLE =~ /my var/ && $EMPTY_VARIABLE =~ /nope/|} ;
  [%expect {|(B false)|}] ;
  test_eval {|$EMPTY_VARIABLE == "" && $PRESENT_VARIABLE|} ;
  [%expect {|(S "my variable")|}] ;
  test_eval {|$EMPTY_VARIABLE == "" && $PRESENT_VARIABLE != "nope"|} ;
  [%expect {|(B true)|}] ;
  test_eval {|$PRESENT_VARIABLE && $EMPTY_VARIABLE|} ;
  [%expect {|(S "")|}] ;
  test_eval {|$PRESENT_VARIABLE && $UNDEFINED_VARIABLE|} ;
  [%expect {|Null|}] ;
  test_eval {|$UNDEFINED_VARIABLE && $EMPTY_VARIABLE|} ;
  [%expect {|Null|}] ;
  test_eval {|$UNDEFINED_VARIABLE && $PRESENT_VARIABLE|} ;
  [%expect {|Null|}] ;
  test_eval
    {|$FULL_PATH_VARIABLE =~ /^\/a\/full\/path\/variable\/value$/ && $PATH_VARIABLE =~ /path\/variable/|} ;
  [%expect {|(B true)|}] ;
  test_eval
    {|$FULL_PATH_VARIABLE =~ /^\/a\/bad\/path\/variable\/value$/ && $PATH_VARIABLE =~ /path\/variable/|} ;
  [%expect {|(B false)|}] ;
  test_eval
    {|$FULL_PATH_VARIABLE =~ /^\/a\/full\/path\/variable\/value$/ && $PATH_VARIABLE =~ /bad\/path\/variable/|} ;
  [%expect {|(B false)|}] ;
  test_eval
    {|$FULL_PATH_VARIABLE =~ /^\/a\/bad\/path\/variable\/value$/ && $PATH_VARIABLE =~ /bad\/path\/variable/|} ;
  [%expect {|(B false)|}] ;
  test_eval
    {|$FULL_PATH_VARIABLE =~ /^\/a\/full\/path\/variable\/value$/ || $PATH_VARIABLE =~ /path\/variable/|} ;
  [%expect {|(B true)|}] ;
  test_eval
    {|$FULL_PATH_VARIABLE =~ /^\/a\/bad\/path\/variable\/value$/ || $PATH_VARIABLE =~ /path\/variable/|} ;
  [%expect {|(B true)|}] ;
  test_eval
    {|$FULL_PATH_VARIABLE =~ /^\/a\/full\/path\/variable\/value$/ || $PATH_VARIABLE =~ /bad\/path\/variable/|} ;
  [%expect {|(B true)|}] ;
  test_eval
    {|$FULL_PATH_VARIABLE =~ /^\/a\/bad\/path\/variable\/value$/ || $PATH_VARIABLE =~ /bad\/path\/variable/|} ;
  [%expect {|(B false)|}] ;
  test_eval {|$PRESENT_VARIABLE =~ /my var/ || $EMPTY_VARIABLE =~ /nope/|} ;
  [%expect {|(B true)|}] ;
  test_eval {|$EMPTY_VARIABLE == "" || $PRESENT_VARIABLE|} ;
  [%expect {|(B true)|}] ;
  test_eval {|$PRESENT_VARIABLE != "nope" || $EMPTY_VARIABLE == ""|} ;
  [%expect {|(B true)|}] ;
  test_eval {|$PRESENT_VARIABLE && null || $EMPTY_VARIABLE == ""|} ;
  [%expect {|(B true)|}] ;
  test_eval {|$PRESENT_VARIABLE || $UNDEFINED_VARIABLE|} ;
  [%expect {|(S "my variable")|}] ;
  test_eval {|$UNDEFINED_VARIABLE || $PRESENT_VARIABLE|} ;
  [%expect {|(S "my variable")|}] ;
  test_eval {|$UNDEFINED_VARIABLE == null || $PRESENT_VARIABLE|} ;
  [%expect {|(B true)|}] ;
  test_eval {|$PRESENT_VARIABLE || $UNDEFINED_VARIABLE == null|} ;
  [%expect {|(S "my variable")|}] ;
  test_eval {|($PRESENT_VARIABLE)|} ;
  [%expect {|(S "my variable")|}] ;
  test_eval {|(($PRESENT_VARIABLE))|} ;
  [%expect {|(S "my variable")|}] ;
  test_eval {|(($PRESENT_VARIABLE && null) || $EMPTY_VARIABLE == "")|} ;
  [%expect {|(B true)|}] ;
  test_eval {|($PRESENT_VARIABLE) && (null || $EMPTY_VARIABLE == "")|} ;
  [%expect {|(B true)|}] ;
  test_eval {|("string" || "test") == "string"|} ;
  [%expect {|(B true)|}] ;
  test_eval {|(null || ("test" == "string"))|} ;
  [%expect {|(B false)|}] ;
  test_eval {|("string" == ("test" && "string"))|} ;
  [%expect {|(B true)|}] ;
  test_eval {|("string" == ("test" || "string"))|} ;
  [%expect {|(B false)|}] ;
  test_eval {|("string" == "test" || "string")|} ;
  [%expect {|(S "string")|}] ;
  test_eval {|("string" == ("string" || (("1" == "1") && ("2" == "3"))))|} ;
  [%expect {|(B true)|}] ;
  (* truthful *)
  test_eval_bool {|$PRESENT_VARIABLE == "my variable"|} ;
  [%expect "true"] ;
  test_eval_bool "$PRESENT_VARIABLE == 'no match'" ;
  [%expect "false"] ;
  test_eval_bool {|$UNDEFINED_VARIABLE == null|} ;
  [%expect "true"] ;
  test_eval_bool {|$PRESENT_VARIABLE|} ;
  [%expect "true"] ;
  test_eval_bool {|$UNDEFINED_VARIABLE|} ;
  [%expect "false"] ;
  test_eval_bool {|$EMPTY_VARIABLE|} ;
  [%expect "false"] ;
  test_eval_bool {|$INVALID = 1|} ;
  [%expect "/!\\ could not parse /!\\"] ;
  test_eval_bool "$PRESENT_VARIABLE =~ /var.*/" ;
  [%expect "true"] ;
  test_eval_bool "$UNDEFINED_VARIABLE =~ /var.*/" ;
  [%expect "false"] ;
  test_eval_bool "$PRESENT_VARIABLE !~ /var.*/" ;
  [%expect "false"] ;
  test_eval_bool "$UNDEFINED_VARIABLE !~ /var.*/" ;
  [%expect "true"]

(* https://docs.gitlab.com/ee/ci/jobs/job_control.html#compare-a-variable-to-a-regex-pattern *)
let%expect_test "test expression evaluation: statement_spec.rb (truthful when \
                 variables have patterns)" =
  let env =
    Env.of_list
      [
        ("teststring", "abcde"); ("pattern1", "/^ab.*/"); ("pattern2", "/^at.*/");
      ]
  in
  let test_eval_bool exp =
    match Expr.parse_opt exp with
    | None -> print_endline "/!\\ could not parse /!\\"
    | Some exp -> test_eval_bool ~env exp
  in
  test_eval_bool {|$teststring =~ "abcde"|} ;
  [%expect "true"] ;
  test_eval_bool {|$teststring =~ $teststring|} ;
  [%expect "true"] ;
  (* test_eval_bool {|$teststring =~ $pattern1|} ; *)
  (* [%expect "true"] ; *)
  test_eval_bool {|$teststring =~ $pattern2|} ;
  [%expect "false"]
