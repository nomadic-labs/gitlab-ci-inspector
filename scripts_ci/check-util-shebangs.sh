#!/bin/sh

res=$(mktemp)
echo "0" > "${res}"
find utils/ -maxdepth 1 -iname \*.sh | while read -r util; do
  if [ "$(head -n1 "$util")" != "#!/bin/sh" ]; then
    echo "/!\ shebang of file '$util' is not '#!/bin/sh'"
    echo "1" > "${res}"
  fi
done

exit_code=$(cat "$res")
rm "$res"
exit "$exit_code"
