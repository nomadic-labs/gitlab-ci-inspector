Test error handling:

  $ ../bin/main.exe --ci-file ../test_confs/invalid/invalid.yml -s plain merge
  ../test_confs/invalid/invalid.yml: Could not parse the file as YAML, message: error calling parser: did not find expected key character 0 position 0 returned: 0.
  
  Unfortunately, GitLab CI Inspector cannot give the precise location of YAML parsing errors. Consider using a separate tool like 'yamllint'. You can invoke it through Docker:
  
  docker run --rm -it -v $(pwd):/data cytopia/yamllint --config-data "{extends: relaxed, rules: {line-length: disable}}" ../test_confs/invalid/invalid.yml
  
  [1]

  $ ../bin/main.exe --ci-file ../test_confs/invalid/include_invalid.yml -s plain merge
  ../test_confs/invalid/invalid.yml: Could not parse the file as YAML, message: error calling parser: did not find expected key character 0 position 0 returned: 0.
  
  Unfortunately, GitLab CI Inspector cannot give the precise location of YAML parsing errors. Consider using a separate tool like 'yamllint'. You can invoke it through Docker:
  
  docker run --rm -it -v $(pwd):/data cytopia/yamllint --config-data "{extends: relaxed, rules: {line-length: disable}}" ../test_confs/invalid/invalid.yml
  
  [1]

TODO: improve the output when the list of definitions is empty.

  $ ../bin/main.exe --ci-file ../test_confs/invalid/template_not_found.yml focus foo
  Could not find template 'bar' when resolving 'extends' in job 'foo', available definitions:
  
  
  [1]

TODO: why not print the filename instead of '(top-level)' ?

  $ ../bin/main.exe --ci-file ../test_confs/invalid/include_not_found.yml merge
  (top-level): Could not find included file 'does_not_exist.yml'
  [1]

TODO: Print this error better and add context (file, job?).

  $ OCAMLRUNPARAM=b=0 ../bin/main.exe --ci-file ../test_confs/invalid/rule_parse_error.yml --settings ../example_settings/gci-example.json simulate job job_foo in push
  Fatal error: exception Conflang.Parse.Error("rule", "Parse error raised when parsing 'if:' of 'rule:', could not parse '$FOO == ' in expression '$FOO == '", _)
  [2]
