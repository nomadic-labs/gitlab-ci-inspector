Test the environments commands:

  $ ../bin/main.exe --ci-file ../test_confs/small/.gitlab-ci-small.yml --settings ../example_settings/gci-example.json show environments
  Available environments:
  Environment 'double-pipeline'.
  Environment 'master'.
  Environment 'merge-request'.
  Environment 'schedule'.
  Environment 'push'.
  Environment 'empty-env'.
  $ ../bin/main.exe --ci-file ../test_confs/small/.gitlab-ci-small.yml --settings ../example_settings/gci-example.json show environments -f terse
  Available environments:
  Environment 'double-pipeline'.
  Environment 'master'.
  Environment 'merge-request'.
  Environment 'schedule'.
  Environment 'push'.
  Environment 'empty-env'.
  $ ../bin/main.exe --ci-file ../test_confs/small/.gitlab-ci-small.yml --settings ../example_settings/gci-example.json show environments -f verbose
  Available environments:
  Environment 'double-pipeline':
   - CI_COMMIT_BRANCH: 'arvid@phony-branch'
   - CI_OPEN_MERGE_REQUESTS: '1234'
  Environment 'master':
   - CI_PROJECT_NAMESPACE: 'tezos'
   - CI_COMMIT_REF_NAME: 'master'
   - CI_COMMIT_BRANCH: 'master'
   - CI_PIPELINE_SOURCE: 'push'
  Environment 'merge-request':
   - CI_PIPELINE_SOURCE: 'merge_request_event'
  Environment 'schedule':
   - CI_PIPELINE_SOURCE: 'schedule'
  Environment 'push':
   - CI_PIPELINE_SOURCE: 'push'
  Environment 'empty-env':
  $ ../bin/main.exe --ci-file ../test_confs/small/.gitlab-ci-small.yml --settings ../example_settings/gci-example.json show environments -f json
  {
    "environments": {
      "double-pipeline": {
        "CI_COMMIT_BRANCH": "arvid@phony-branch",
        "CI_OPEN_MERGE_REQUESTS": "1234"
      },
      "master": {
        "CI_PROJECT_NAMESPACE": "tezos",
        "CI_COMMIT_REF_NAME": "master",
        "CI_COMMIT_BRANCH": "master",
        "CI_PIPELINE_SOURCE": "push"
      },
      "merge-request": { "CI_PIPELINE_SOURCE": "merge_request_event" },
      "schedule": { "CI_PIPELINE_SOURCE": "schedule" },
      "push": { "CI_PIPELINE_SOURCE": "push" },
      "empty-env": {}
    }
  }

  $ ../bin/main.exe --ci-file ../test_confs/small/.gitlab-ci-small.yml --settings ../example_settings/gci-example.json show environment double-pipeline
  Environment 'double-pipeline':
   - CI_COMMIT_BRANCH: 'arvid@phony-branch'
   - CI_OPEN_MERGE_REQUESTS: '1234'
  $ ../bin/main.exe --ci-file ../test_confs/small/.gitlab-ci-small.yml --settings ../example_settings/gci-example.json show environment double-pipeline -f terse
  Environment 'double-pipeline':
   - CI_COMMIT_BRANCH: 'arvid@phony-branch'
   - CI_OPEN_MERGE_REQUESTS: '1234'
  $ ../bin/main.exe --ci-file ../test_confs/small/.gitlab-ci-small.yml --settings ../example_settings/gci-example.json show environment double-pipeline -f verbose
  Environment 'double-pipeline':
   - CI_COMMIT_BRANCH: 'arvid@phony-branch'
   - CI_OPEN_MERGE_REQUESTS: '1234'
  $ ../bin/main.exe --ci-file ../test_confs/small/.gitlab-ci-small.yml --settings ../example_settings/gci-example.json show environment double-pipeline -f json
  {
    "CI_COMMIT_BRANCH": "arvid@phony-branch",
    "CI_OPEN_MERGE_REQUESTS": "1234"
  }


Environments with changes:

  $ cat <<'EOT' > gci-changes.json
  > {
  >   "environments": {
  >     "test-environment": {"__changes": ["foo.ml", "bar.ml"]}
  >   }
  > }
  > EOT
  $ ../bin/main.exe --ci-file ../test_confs/small/.gitlab-ci-small.yml --settings gci-changes.json show environment test-environment -f json
  { "__changes": [ "bar.ml", "foo.ml" ] }
  $ ../bin/main.exe --ci-file ../test_confs/small/.gitlab-ci-small.yml --settings gci-changes.json show environment test-environment -f verbose
  Environment 'test-environment':
   - Changes: { "bar.ml", "foo.ml" }
