No CI file:
  $ ../bin/main.exe merge
  Could not find an existing CI configuration file in the current directory, pass '--ci-file'.
  [1]

Phony CI file:
  $ touch .gitlab-ci.yml
  $ ../bin/main.exe merge
  Invalid GitLab CI file .gitlab-ci.yml, it should contain a YAML object.
  [1]
  $ rm .gitlab-ci.yml

Working default CI file:
  $ cp ../test_confs/small/.gitlab-ci-small.yml .gitlab-ci.yml
  $ ../bin/main.exe -s plain merge
  sanity:
    image: ${build_deps_image_name}:runtime-build-test-dependencies--${build_deps_image_version}
    script:
    - scripts/check_opam_test.sh
  $ rm .gitlab-ci.yml
