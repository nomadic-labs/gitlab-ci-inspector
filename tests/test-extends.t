  $ ../bin/main.exe --ci-file ../test_confs/small/.gitlab-ci-multiple-extends.yml -s plain merge
  .foo1:
    bar1: baz1
  .foo2:
    bar2: baz2
  .foo3:
    bar1: baz2
  main:
    extends:
    - .foo1
    - .foo2
    - .foo3
  $ ../bin/main.exe --ci-file ../test_confs/small/.gitlab-ci-multiple-extends.yml -s plain merge --template-extension no-expansion 
  .foo1:
    bar1: baz1
  .foo2:
    bar2: baz2
  .foo3:
    bar1: baz2
  main:
    extends:
    - .foo1
    - .foo2
    - .foo3
  $ ../bin/main.exe --ci-file ../test_confs/small/.gitlab-ci-multiple-extends.yml -s plain merge --template-extension expand 
  .foo1:
    bar1: baz1
  .foo2:
    bar2: baz2
  .foo3:
    bar1: baz2
  main:
    bar2: baz2
    bar1: baz2
  $ ../bin/main.exe --ci-file ../test_confs/small/.gitlab-ci-multiple-extends.yml -s plain merge --template-extension debug-expansion 
  .foo1:
    bar1: baz1
  .foo2:
    bar2: baz2
  .foo3:
    bar1: baz2
  main:
    __gci_extends_begin_.foo1_: begin
    __gci_extends___end_.foo1_: end
    __gci_extends_begin_.foo2_: begin
    bar2: baz2
    __gci_extends___end_.foo2_: end
    __gci_extends_begin_.foo3_: begin
    bar1: baz2
    __gci_extends___end_.foo3_: end
    __gci_job_definition: main
