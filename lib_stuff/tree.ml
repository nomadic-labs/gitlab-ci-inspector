type ('a, 'b) tree = Leaf of 'a | Node of ('b * ('a, 'b) tree list)

let pp_fancy pp_leaf pp_node fmt tree =
  let rec aux ~left fmt tree =
    match tree with
    | Leaf leaf_contents -> Format.fprintf fmt "%a\n" pp_leaf leaf_contents
    | Node (node_contents, leaves) ->
        Format.fprintf fmt "%a\n" pp_node node_contents ;
        List.iteri
          (fun i l ->
            let last_leaf = i = List.length leaves - 1 in
            let turnstile = if last_leaf then "└──" else "├──" in
            match l with
            | Leaf _ -> Format.fprintf fmt "%s%s%a" left turnstile (aux ~left) l
            | Node _ ->
                let next_left = left ^ if last_leaf then "   " else "│  " in
                Format.fprintf
                  fmt
                  "%s%s%a"
                  left
                  turnstile
                  (aux ~left:next_left)
                  l)
          leaves
  in
  aux ~left:"" fmt tree

let%expect_test "test parsing" =
  let pp fmt t = pp_fancy Format.pp_print_int Format.pp_print_string fmt t in
  let print t = Format.printf "%a" pp t in
  let ex1 = Node ("Foo", [Leaf 1; Leaf 2]) in
  print ex1 ;
  [%expect {|
    Foo
    ├──1
    └──2 |}] ;
  let ex2 = Node ("Foo", [Leaf 1; Leaf 2; Node ("Bar", [Leaf 3])]) in
  print ex2 ;
  [%expect {|
    Foo
    ├──1
    ├──2
    └──Bar
       └──3 |}] ;
  let ex3 =
    Node
      ("Foo", [Leaf 1; Node ("Bar", [Leaf 2; Leaf 4]); Node ("Baz", [Leaf 3])])
  in
  print ex3 ;
  [%expect
    {|
    Foo
    ├──1
    ├──Bar
    │  ├──2
    │  └──4
    └──Baz
       └──3 |}] ;
  let ex4 =
    Node
      ( "Foo",
        [
          Leaf 1;
          Leaf 2;
          Node
            ( "Bar",
              [Leaf 3; Node ("Gaz", [Leaf 4; Leaf 5]); Node ("Daz", [Leaf 7])]
            );
          Leaf 6;
        ] )
  in
  print ex4 ;
  [%expect
    {|
    Foo
    ├──1
    ├──2
    ├──Bar
    │  ├──3
    │  ├──Gaz
    │  │  ├──4
    │  │  └──5
    │  └──Daz
    │     └──7
    └──6 |}]

let rec iter (f_leaf : 'a -> unit) (f_node : 'b -> unit) = function
  | Leaf lc -> f_leaf lc
  | Node (nc, children) ->
      f_node nc ;
      List.iter (iter f_leaf f_node) children

let iter_d (f_leaf : int -> 'a -> unit) (f_node : int -> 'b -> unit) t =
  let rec iter_daux depth = function
    | Leaf lc -> f_leaf depth lc
    | Node (nc, children) ->
        f_node depth nc ;
        List.iter (iter_daux (succ depth)) children
  in
  iter_daux 0 t

let pp ?(indent_width = 2) pp_leaf pp_node fmt =
  let indent_string ind = String.make (indent_width * ind) ' ' in
  iter_d
    (fun depth leaf_contents ->
      Format.fprintf fmt "%s- %a\n" (indent_string depth) pp_leaf leaf_contents)
    (fun depth node_contents ->
      Format.fprintf fmt "%s- %a\n" (indent_string depth) pp_node node_contents)
