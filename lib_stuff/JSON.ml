let of_assoc_exn (v : Yojson.Basic.t) =
  match v with `Assoc l -> l | _ -> Stdlib.failwith "not assoc"

let of_assoc_opt (v : Yojson.Basic.t) =
  match v with `Assoc l -> Some l | _ -> None

let of_string_exn (v : Yojson.Basic.t) =
  match v with `String s -> s | _ -> Stdlib.failwith "not string"

let of_string_opt (v : Yojson.Basic.t) =
  match v with `String s -> Some s | _ -> None

let of_list_exn (v : Yojson.Basic.t) =
  match v with `List l -> l | _ -> Stdlib.failwith "not list"

let of_list_opt (v : Yojson.Basic.t) =
  match v with `List l -> Some l | _ -> None

let get_field_exn (v : Yojson.Basic.t) f =
  try List.assoc f (of_assoc_exn v)
  with Not_found ->
    Stdlib.failwith
      (Format.asprintf
         "not field %s in `%a`"
         f
         (Yojson.Basic.pretty_print ~std:true)
         v)

let get_field_opt (v : Yojson.Basic.t) f =
  Option.bind (of_assoc_opt v) (List.assoc_opt f)

let get (v : Yojson.Basic.t) f =
  Option.value (Option.bind (of_assoc_opt v) (List.assoc_opt f)) ~default:`Null

let ( |-> ) v f = get v f
