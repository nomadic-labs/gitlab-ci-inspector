type log_level = Quiet | Error | Warn | Report | Info | Debug

let current_log_level = ref Info

let color_enabled = ref true

let log_level_to_string =
  let color clr str = Color.(apply ~color_enabled:!color_enabled clr str) in
  function
  | Quiet -> invalid_arg "Log.log_level_to_string: level cannot be Quiet"
  | Error -> Some (color Color.FG.red "Error")
  | Warn -> Some (color Color.FG.yellow "Warning")
  | Info -> Some "Info"
  | Debug -> Some "Debug"
  | Report -> None

let log_string ~(level : log_level) message =
  match (!current_log_level, level) with
  | _, Quiet -> invalid_arg "Log.log_string: level cannot be Quiet"
  | Error, Error
  | Warn, (Error | Warn)
  | Report, (Error | Warn | Report)
  | Info, (Error | Warn | Report | Info)
  | Debug, (Error | Warn | Report | Info | Debug) ->
      Format.printf
        "%s%s\n"
        (match log_level_to_string level with
        | Some level_prefix -> level_prefix ^ ": "
        | None -> "")
        message ;
      Format.print_flush ()
  | (Quiet | Error | Warn | Report | Info), _ -> ()

let log ~level fmt = Format.kasprintf (log_string ~level) fmt

let debug fmt = log ~level:Debug fmt

let info x = log ~level:Info x

let report x = log ~level:Report x

let warn x = log ~level:Warn x

let error x = log ~level:Error x
