let emojis = function
  | `Green_circle -> "🟢"
  | `Orange_circle -> "🟠"
  | `Green_square -> "🟩"
  | `Gears -> "⚙"
  | `Clock -> "🕜"
  | `Red_square -> "🟥"
