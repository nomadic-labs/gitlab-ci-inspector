open Util

let tag = function
  | `A _ -> "array"
  | `Bool _ -> "bool"
  | `Float _ -> "float"
  | `Null -> "null"
  | `O _ -> "object"
  | `String _ -> "string"

let of_string_opt v = match v with `String s -> Some s | _ -> None

let of_string_exn ?err v =
  match v with
  | `String s -> s
  | _ ->
      failwith
      @@ Option.value err ~default:"Expected string"
      ^ ", when reading: "
      ^ Yaml.to_string_exn ~scalar_style:`Plain v

let of_object_opt v = match v with `O l -> Some l | _ -> None

let of_object_exn ?err v =
  match of_object_opt v with
  | Some l -> l
  | _ ->
      failwith
      @@ Option.value err ~default:"Expected object"
      ^ ", when reading: "
      ^ Yaml.to_string_exn ~scalar_style:`Plain v

let object_filter_keys v f = `O (List.filter f (of_object_exn v))

let of_array_opt v = match v with `A l -> Some l | _ -> None

let of_array_exn ?err v =
  match of_array_opt v with
  | Some l -> l
  | None ->
      failwith
      @@ Option.value err ~default:"Expected array"
      ^ ", when reading: "
      ^ Yaml.to_string_exn ~scalar_style:`Plain v

let of_array ?(default = []) v = Option.value ~default (of_array_opt v)

let of_string_or_array ?(default = []) ?err v =
  match v with
  | `A l -> List.map of_string_exn l
  | `String s -> [s]
  | `Null -> default
  | _ ->
      failwith
      @@ Option.value err ~default:"Expected string or array of strings"
      ^ ", when reading: "
      ^ Yaml.to_string_exn ~scalar_style:`Plain v

let of_bool_opt v = match v with `Bool b -> Some b | _ -> None

let of_bool_exn ?err v =
  match of_bool_opt v with
  | Some l -> l
  | None ->
      failwith
      @@ Option.value err ~default:"Expected boolean"
      ^ ", when reading: "
      ^ Yaml.to_string_exn ~scalar_style:`Plain v

let of_bool ?(default = false) v = Option.value ~default (of_bool_opt v)

let of_float_opt v = match v with `Float f -> Some f | _ -> None

let get ?(default = `Null) name (node : Yaml.value) =
  match node with
  | `O fields -> (
      match List.assoc_opt name fields with None -> `Null | Some node -> node)
  | _ -> default

let has name (node : Yaml.value) =
  match node with `O fields -> List.mem_assoc name fields | _ -> false

let ( |-> ) node field = get field node

let concat_objects (os : Yaml.value list) : Yaml.value =
  `O (List.concat @@ List.map of_object_exn os)

let merge_objects (os : Yaml.value list) : Yaml.value =
  let fields = List.concat @@ List.map of_object_exn os in
  let tbl = Hashtbl.create 30 in
  List.iter (fun (k, v) -> Hashtbl.replace tbl k v) fields ;
  `O (List.of_seq @@ Hashtbl.to_seq tbl)

let rec replace_assoc_inplace k v = function
  | [] -> [(k, v)]
  | (k', v') :: tl ->
      if k = k' then (k, v) :: tl else (k', v') :: replace_assoc_inplace k v tl

let object_set (k : string) (v : Yaml.value) (o : Yaml.value) : Yaml.value =
  let fields = of_object_exn o in
  `O (replace_assoc_inplace k v fields)

let object_rem (k : string) (o : Yaml.value) : Yaml.value =
  let fields = of_object_exn o in
  `O (List.remove_assoc k fields)

let object_prepend_field (k : string) (v : Yaml.value) (o : Yaml.value) :
    Yaml.value =
  let fields = of_object_exn o in
  `O ((k, v) :: fields)

let object_append_field (k : string) (v : Yaml.value) (o : Yaml.value) :
    Yaml.value =
  let fields = of_object_exn o in
  `O (fields @ [(k, v)])

let merge_objects_inplace (os : Yaml.value list) : Yaml.value =
  let fields = List.concat @@ List.map of_object_exn os in
  let fields_merged =
    List.fold_left
      (fun fields' (k, v) -> replace_assoc_inplace k v fields')
      []
      fields
  in
  `O fields_merged

let rec merge_objects_append ?(recursive = false) (os : Yaml.value list) :
    Yaml.value =
  let fields = List.concat @@ List.map of_object_exn os in
  let fields_merged =
    List.fold_left
      (fun fields' (k, v) -> replace_assoc_append ~recursive k v fields')
      []
      fields
  in
  `O fields_merged

and replace_assoc_append ?(recursive = false) k' v' = function
  | [] -> [(k', v')]
  | (k, v) :: tl ->
      if k = k' then
        let v'' =
          match (v, v') with
          | `O _o, `O _o' when recursive ->
              merge_objects_append ~recursive [v; v']
          | _ -> v'
        in
        tl @ [(k', v'')]
      else (k, v) :: replace_assoc_append ~recursive k' v' tl

let rec sort_keys_recursively (v : Yaml.value) : Yaml.value =
  match v with
  | `O bindings ->
      let bindings =
        List.map (fun (k, v) -> (k, sort_keys_recursively v)) bindings
      in
      `O (List.sort (fun (k, _) (k', _) -> String.compare k k') bindings)
  | `A l -> `A (List.map sort_keys_recursively l)
  | _ -> v

(** Raised if parsing an included file as YAML fails *)
exception Yaml_file_parse_error of {path : string; message : string}

let of_fpath_exn fpath =
  match Yaml_unix.of_file fpath with
  | Ok v -> v
  | Error (`Msg message) ->
      raise (Yaml_file_parse_error {path = Fpath.to_string fpath; message})

let of_fpath fpath =
  let open Result_util.Syntax in
  try return (of_fpath_exn fpath)
  with Yaml_file_parse_error {path; message} ->
    fail
      (sf
         "yaml_of_file: Could not parse YAML in file '%s', message %s"
         path
         message)

let of_file path =
  let open Result_util.Syntax in
  let* fpath =
    match Fpath.of_string path with
    | Ok path -> return path
    | Error (`Msg m) -> fail (sf "yaml_of_file: Invalid path '%s': %s" path m)
  in
  of_fpath fpath

let of_file_exn path =
  let fpath =
    match Fpath.of_string path with
    | Ok path -> path
    | Error (`Msg m) ->
        raise
          (Invalid_argument (sf "yaml_of_file: Invalid path '%s': %s" path m))
  in
  of_fpath_exn fpath
