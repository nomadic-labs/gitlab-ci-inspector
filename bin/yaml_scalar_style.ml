(*****************************************************************************)
(*                                                                           *)
(* SPDX-License-Identifier: MIT                                              *)
(* Copyright (c) 2023 Nomadic Labs. <contact@nomadic-labs.com>               *)
(*                                                                           *)
(*****************************************************************************)

let all : (string * Yaml.scalar_style) list =
  [
    ("plain", `Plain);
    ("single-quoted", `Single_quoted);
    ("any", `Any);
    ("double-quoted", `Double_quoted);
    ("folded", `Folded);
    ("literal", `Literal);
  ]

let of_string_opt s = List.assoc_opt s all

let to_string = function
  | `Plain -> "plain"
  | `Single_quoted -> "single-quoted"
  | `Any -> "any"
  | `Double_quoted -> "double-quoted"
  | `Folded -> "folded"
  | `Literal -> "literal"
