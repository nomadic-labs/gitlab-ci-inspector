open Tezos_error_monad
open Error_monad.Lwt_result_syntax
open Stuff
open Stuff.Base

let group = Tezos_clic.{name = "gitlab"; title = "Gitlab CI file commands"}

module GlClic = struct
  (* Parameters *)
  let ci_file_parameter =
    Tezos_clic.(
      parameter @@ fun (_ctxt : Context.t) p ->
      match Fpath.of_string p with
      | Ok path -> return path
      | Error (`Msg error_message) ->
          error "Invalid CI configuration file path: %s, %s" p error_message)

  let settings_file_parameter =
    Tezos_clic.(
      parameter @@ fun (_ctxt : Context.t) p ->
      match Fpath.of_string p with
      | Ok path -> return path
      | Error (`Msg error_message) ->
          error "Invalid settings file path: %s, %s" p error_message)

  let int_parameter =
    Tezos_clic.(
      parameter @@ fun (_ctxt : Context.t) p -> return @@ int_of_string p)

  let environment_parameter :
      (Config_environment.t, Context.t) Tezos_clic.parameter =
    Tezos_clic.(
      parameter @@ fun (ctxt : Context.t) p ->
      match Hashtbl.find_opt ctxt.settings.environments p with
      | Some e -> return e
      | None ->
          error
            "The GitLab CI Inspector configuration does not contain \
             environment: %s. Available environments:\n\
             @[<v>%a@]"
            p
            (Format.pp_print_seq
               ~pp_sep:Format.pp_print_newline
               Format.pp_print_string)
            (ctxt.settings.environments |> Hashtbl.to_seq_keys))

  let environment_arg =
    Tezos_clic.arg
      ~doc:"Load environment from config file"
      ~short:'e'
      ~long:"environment"
      ~placeholder:"env"
      environment_parameter

  let evaluation_strategy_parameter :
      (Commands.inclusion_strategy_named, Context.t) Tezos_clic.parameter =
    Tezos_clic.(
      parameter @@ fun (ctxt : Context.t) param ->
      match List.assoc_opt param Commands.evaluation_strategies with
      | Some strategy -> return strategy
      | None -> (
          match Hashtbl.find_opt ctxt.settings.environments param with
          | Some environment -> return (Commands.Per_named_env environment)
          | None ->
              error
                "'%s' is not a valid evaluation strategy. It should be one of \
                 the strategies:@.@[<v>%a@]@.or one of the \
                 environments:@.@[<v>%a@]"
                param
                (Format.pp_print_list
                   ~pp_sep:Format.pp_print_newline
                   Format.pp_print_string)
                (List.map fst Commands.evaluation_strategies)
                (Format.pp_print_seq
                   ~pp_sep:Format.pp_print_newline
                   Format.pp_print_string)
                (ctxt.settings.environments |> Hashtbl.to_seq_keys)))

  let evaluation_strategy_arg ?(default = "always-include") () =
    Tezos_clic.default_arg
      ~doc:
        "One of 'always-include' (the default behavior: include all \
         conditional includes), 'never-include' (never include conditional \
         includes), 'empty-env' (evaluate include conditions with empty \
         environment) or <ENVIRONMENT> (evaluate include conditions with the \
         given environment)."
      ~long:"evaluation-strategy"
      ~short:'e'
      ~placeholder:"strat"
      ~default
      evaluation_strategy_parameter

  let job_parameter :
      ( string * (Gitlab.template_extensions_strategy -> Yaml.value),
        Context.t )
      Tezos_clic.parameter =
    Tezos_clic.(
      parameter @@ fun (ctxt : Context.t) job_name ->
      let ci_config = Gitlab.merge ctxt.base_dir ctxt.ci_config in
      let Gitlab.{jobs; _} = Gitlab.partition ci_config in
      match List.assoc_opt job_name jobs with
      | Some job ->
          return
          @@ ( job_name,
               function
               | Gitlab.No_expansion -> job
               | template_extensions_strategy ->
                   let ci_config =
                     Gitlab.merge
                       ~template_extensions_strategy
                       ctxt.base_dir
                       ctxt.ci_config
                   in
                   let Gitlab.{jobs; _} = Gitlab.partition ci_config in
                   List.assoc job_name jobs )
      | None ->
          error
            "The passed Gitlab CI config file does not define a job `%s`. \
             Available jobs:\n\
             @[<v>%a@]"
            job_name
            (Format.pp_print_list
               ~pp_sep:Format.pp_print_newline
               Format.pp_print_string)
            (List.map fst jobs))

  let gitlab_token_parameter =
    Tezos_clic.(
      parameter @@ fun (_ctxt : Context.t) access_token ->
      return (Some access_token))

  let scalar_style_parameter =
    Tezos_clic.(
      parameter @@ fun (_ctxt : Context.t) param ->
      match Yaml_scalar_style.of_string_opt param with
      | Some style -> return style
      | None ->
          error
            "Unknown quoting style: '%s'. Valid choices: %s"
            param
            (String.concat ", " @@ List.map fst Yaml_scalar_style.all))

  let extension_strategy_parameter =
    Tezos_clic.(
      parameter @@ fun (_ctxt : Context.t) param ->
      match List.assoc_opt param Commands.extension_strategies with
      | Some strategy -> return strategy
      | None ->
          error
            "Unknown extension strategy: '%s'. Valid choices: %s"
            param
            (String.concat ", " @@ List.map fst Commands.extension_strategies))

  (* Args *)
  let template_extensions_strategy_arg ~default =
    Tezos_clic.default_arg
      ~doc:
        (Format.asprintf
           "Template extension strategy used. Valid choices: %s"
           (String.concat ", " @@ List.map fst Commands.extension_strategies))
      ~short:'t'
      ~long:"template-extension"
      ~placeholder:"strategy"
      ~default
      extension_strategy_parameter
end

module Global_options = struct
  let access_token =
    Tezos_clic.arg
      ~doc:"Gitlab Access token"
      ~short:'t'
      ~long:"gl-access-token"
      ~placeholder:"<TOKEN>"
      GlClic.gitlab_token_parameter

  let gitlab_project =
    Tezos_clic.(
      arg
        ~doc:"GitLab project"
        ~short:'p'
        ~long:"gl-project"
        ~placeholder:"namespace/project"
        (parameter @@ fun _ project -> return project))

  let scalar_style =
    Tezos_clic.arg
      ~doc:
        (Format.asprintf
           "Scalar style used. Valid choices: %s. Default: '%s'."
           (String.concat ", " @@ List.map fst Yaml_scalar_style.all)
           (Yaml_scalar_style.to_string Context.default.settings.scalar_style))
      ~short:'s'
      ~long:"scalar-style"
      ~placeholder:"style"
      GlClic.scalar_style_parameter

  let sort_keys_arg =
    Tezos_clic.switch
      ~doc:"Sort keys of YAML hashes in output"
      ~short:'k'
      ~long:"sort-keys"
      ()

  let ci_file =
    Tezos_clic.arg
      ~doc:"The .gitlab-ci.yml file to act on (or directory containing it)"
      ~short:'f'
      ~long:"ci-file"
      ~placeholder:".gitlab-ci.yml"
      GlClic.ci_file_parameter

  let settings_file =
    Tezos_clic.arg
      ~doc:"The GitLab CI Inspector settings file"
      ~short:'S'
      ~long:"settings"
      ~placeholder:".gci.json"
      GlClic.settings_file_parameter

  let log_level_debug =
    Tezos_clic.switch ~doc:"Debug log level" ~short:'d' ~long:"debug" ()

  let log_level_info =
    Tezos_clic.switch ~doc:"Info log level" ~short:'i' ~long:"info" ()

  let options =
    Tezos_clic.args8
      access_token
      ci_file
      settings_file
      scalar_style
      sort_keys_arg
      log_level_debug
      log_level_info
      gitlab_project
end

module Action_focus = struct
  let options =
    let arg_no_preamble =
      Tezos_clic.switch
        ~doc:"Do not print the CI preamble"
        ~short:'p'
        ~long:"no-preamble"
        ()
    in
    let arg_no_needs =
      Tezos_clic.switch
        ~doc:"Strip any needs or dependencies fields in the focused job"
        ~short:'n'
        ~long:"no-needs"
        ()
    in
    let arg_no_retry =
      Tezos_clic.switch
        ~doc:"Strip any retry field in the focused job"
        ~short:'r'
        ~long:"no-retry"
        ()
    in
    Tezos_clic.args4
      arg_no_preamble
      arg_no_needs
      arg_no_retry
      (GlClic.template_extensions_strategy_arg ~default:"expand")

  let params =
    Tezos_clic.(
      prefix "focus"
      @@ param ~name:"job" ~desc:"Job to focus" GlClic.job_parameter
      @@ stop)

  let command =
    Tezos_clic.command
      ~desc:"Focus a job"
      ~group
      options
      params
      (fun options params context ->
        Commands.Focus.focus options params context ;
        return_unit)
end

module Action_lint = struct
  let options =
    let include_merged_yaml =
      Tezos_clic.switch
        ~doc:"Output the merged yaml"
        ~short:'m'
        ~long:"include-merged-yaml"
        ()
    in
    Tezos_clic.args2 include_merged_yaml (GlClic.evaluation_strategy_arg ())

  let params = Tezos_clic.(prefix "lint" @@ stop)

  let command =
    Tezos_clic.command
      ~desc:"Lint CI file"
      ~group
      options
      params
      (fun options context ->
        let*! () = Commands.Lint.lint options context in
        return_unit)
end

module Action_simulate = struct
  let cutoff_arg =
    Tezos_clic.arg
      ~doc:
        "Show only first (if positive) or last (if negative) <n> jobs per stage"
      ~short:'c'
      ~long:"cutoff"
      ~placeholder:"n"
      GlClic.int_parameter

  let environment_param =
    Tezos_clic.param
      ~name:"environment"
      ~desc:"Pipeline execution environment"
      GlClic.environment_parameter

  let command_simulate_job =
    Tezos_clic.command
      ~desc:"Simulate job execution"
      ~group
      Tezos_clic.no_options
      Tezos_clic.(
        prefixes ["simulate"; "job"]
        @@ param ~name:"job" ~desc:"Job to simulate" GlClic.job_parameter
        @@ prefix "in" @@ environment_param @@ Tezos_clic.stop)
      (fun () job conf_env context ->
        Commands.Simulate.simulate_job job conf_env context ;
        return_unit)

  let command_simulate_pipeline =
    Tezos_clic.command
      ~desc:"Simulate pipeline execution"
      ~group
      Tezos_clic.no_options
      Tezos_clic.(
        prefixes ["simulate"; "pipeline"; "in"] @@ environment_param @@ stop)
      (fun () conf_env context ->
        Commands.Simulate.simulate_pipeline conf_env context ;
        return_unit)

  let command_visualize_pipeline =
    Tezos_clic.command
      ~desc:"Visualize pipeline"
      ~group
      (Tezos_clic.args1 cutoff_arg)
      Tezos_clic.(prefixes ["visualize"; "pipeline"] @@ stop)
      (fun cutoff context ->
        Commands.Simulate.visualize_pipeline cutoff context ;
        return_unit)

  let format_environments_arg :
      (Commands.Environments.environment_format, Context.t) Tezos_clic.arg =
    Tezos_clic.(
      default_arg
        ~doc:
          "How to format environments: 'terse' (default, shows only names), \
           'verbose' (shows names and variables), 'json' (JSON output \
           compatible with '.gci.json' file"
        ~short:'f'
        ~placeholder:"format"
        ~default:"terse"
        ~long:"format"
        ( parameter @@ fun _ctxt param ->
          match Commands.Environments.environment_format_of_string param with
          | Some format -> return format
          | None ->
              error
                "Unknown format type '%s'. Available formats are 'terse', \
                 'verbose' and 'json'."
                param ))

  let command_show_environment =
    Tezos_clic.command
      ~desc:"Show environment"
      ~group
      (Tezos_clic.args1 format_environments_arg)
      Tezos_clic.(prefixes ["show"; "environment"] @@ environment_param @@ stop)
    @@ fun format env (_ : Context.t) ->
    Commands.Environments.show_environment format env ;
    return_unit

  let command_show_environments =
    Tezos_clic.command
      ~desc:"Show environments"
      ~group
      (Tezos_clic.args1 format_environments_arg)
      Tezos_clic.(prefixes ["show"; "environments"] @@ stop)
    @@ fun format context ->
    Commands.Environments.show_environments format context ;
    return_unit

  let command_merge =
    let options =
      let exclude_templates_switch =
        Tezos_clic.switch
          ~doc:"Exclude templates from merged output"
          ~short:'x'
          ~long:"exclude-templates"
          ()
      in
      let exclude_untriggered_switch =
        Tezos_clic.switch
          ~doc:"Exclude untriggered jobs merged output"
          ~short:'X'
          ~long:"exclude-untriggered-jobs"
          ()
      in
      let strip_job_keys =
        Tezos_clic.(
          default_arg
            ~doc:
              "A comma-separated list of keys to strip from jobs (if present)"
            ~long:"strip-job-keys"
            ~placeholder:"key1,key2,..."
            ~default:""
            (Tezos_clic.parameter (fun _ctxt param ->
                 match String.split_on_char ',' param with
                 | [] -> error "Empty list of keys passed."
                 | keys -> return keys)))
      in
      Tezos_clic.args5
        (GlClic.template_extensions_strategy_arg ~default:"no-expansion")
        (GlClic.evaluation_strategy_arg ())
        exclude_templates_switch
        exclude_untriggered_switch
        strip_job_keys
    in
    let params = Tezos_clic.(prefix "merge" @@ stop) in
    Tezos_clic.command
      ~desc:"Merge CI file"
      ~group
      options
      params
      (fun options context ->
        Commands.Simulate.merge options context ;
        return_unit)

  let commands =
    [
      command_visualize_pipeline;
      command_simulate_job;
      command_simulate_pipeline;
      command_show_environment;
      command_show_environments;
      command_merge;
    ]
end

module Action_shellcheck = struct
  let options = Tezos_clic.no_options

  let commands =
    [
      Tezos_clic.command
        ~desc:"Shellcheck a job"
        ~group
        options
        Tezos_clic.(
          prefixes ["shellcheck"; "job"]
          @@ param ~name:"job" ~desc:"Job to shellcheck" GlClic.job_parameter
          @@ stop)
        (fun () job context ->
          let*! () = Commands.Shellcheck.one_job job context in
          return_unit);
      Tezos_clic.command
        ~desc:"Shellcheck all jobs"
        ~group
        options
        Tezos_clic.(prefixes ["shellcheck"; "all"; "jobs"] @@ stop)
        (fun () context ->
          let*! () = Commands.Shellcheck.all_jobs context in
          return_unit);
      Tezos_clic.command
        ~desc:"Exec a job"
        ~group
        (Tezos_clic.args1 GlClic.environment_arg)
        Tezos_clic.(
          prefixes ["exec"; "job"]
          @@ string ~name:"job" ~desc:"Job to execute"
          @@ stop)
        (fun conf_env job context ->
          let*! () = Commands.Shellcheck.exec_one_job conf_env job context in
          return_unit);
    ]
end

module Action_visualize_templates = struct
  let template_param =
    Tezos_clic.(
      param
        ~name:"job/template"
        ~desc:"Job or template (hidden job)."
        (parameter (fun _ctxt param -> return param)))

  let reverse_arg =
    Tezos_clic.switch
      ~doc:"Child nodes are extended templates instead of extending jobs."
      ~short:'r'
      ~long:"reverse"
      ()

  let no_prune_arg =
    Tezos_clic.switch
      ~doc:
        "Disabling pruning of duplicated subtrees. If set, the complete \
         extension tree starting from each template or job is visualized. If \
         omitted, a job or template that already exists as a subtree of an \
         previously printed tree are not printed."
      ~short:'p'
      ~long:"no-prune"
      ()

  let command =
    Tezos_clic.command
      ~desc:
        "Visualize template hierarchy as a forest. 'visualize templates <job>' \
         visualizes the set of jobs or templates that extend <job>. \
         Conversely, 'visualize templates <job> --reverse' visualizes the set \
         of jobs and templates extended by <job>. If <job> is omitted, all \
         templates and jobs with at least one extendee (resp. extends with \
         --reverse) are visualized."
      ~group
      (Tezos_clic.args2 reverse_arg no_prune_arg)
      Tezos_clic.(
        prefixes ["visualize"; "templates"] @@ seq_of_param template_param)
      (fun args templates context ->
        Commands.Visualize.visualize_templates args templates context ;
        return_unit)
end

let commands =
  [
    Action_focus.command; Action_lint.command; Action_visualize_templates.command;
  ]
  @ Action_simulate.commands @ Action_shellcheck.commands

let commands_with_man =
  Tezos_clic.add_manual
    ~executable_name:(Filename.basename Sys.executable_name)
    ~global_options:Global_options.options
    (if Unix.isatty Unix.stdout then Tezos_clic.Ansi else Tezos_clic.Plain)
    Format.std_formatter
    commands

let () =
  ignore
    Tezos_clic.(
      setup_formatter
        Format.std_formatter
        (if Unix.isatty Unix.stdout then Ansi else Plain)
        Short) ;
  let original_args = Array.to_list Sys.argv |> List.tl in
  let path_opt_to_string = function
    | None -> "(top-level)"
    | Some include_base -> include_base
  in
  match
    Lwt_main.run
      (let* global_args, args =
         Tezos_clic.parse_global_options
           Global_options.options
           Context.default
           original_args
       in
       let ctx = Context.setup global_args in
       Tezos_clic.dispatch commands_with_man ctx args)
  with
  | Ok global_options -> global_options
  | Error [Tezos_clic.Help command] ->
      Tezos_clic.usage
        Format.std_formatter
        ~executable_name:(Filename.basename Sys.executable_name)
        ~global_options:Global_options.options
        (match command with None -> [] | Some c -> [c]) ;
      exit 0
  | Error errors ->
      Tezos_clic.pp_cli_errors
        Format.err_formatter
        ~executable_name:(Filename.basename Sys.executable_name)
        ~global_options:Global_options.options
        ~default:(fun fmt err -> Error_monad.pp_print_trace fmt [err])
        errors ;
      exit 1
  | exception YAML.Yaml_file_parse_error {path; message} ->
      let yamllint_suggestion =
        "docker run --rm -it -v $(pwd):/data cytopia/yamllint --config-data \
         \"{extends: relaxed, rules: {line-length: disable}}\" " ^ path
      in
      error
        "%s: Could not parse the file as YAML, message: %s.\n\n\
         Unfortunately, GitLab CI Inspector cannot give the precise location \
         of YAML parsing errors. Consider using a separate tool like \
         'yamllint'. You can invoke it through Docker:\n\n\
         %s\n"
        path
        message
        yamllint_suggestion
  | exception
      Gitlab.Template_not_found
        {path = _; template_name; job_name; available_templates; job_source = _}
    ->
      error
        "Could not find template '%s' when resolving 'extends' in job '%s', \
         available definitions:\n\
         @[<v>%a@]"
        template_name
        job_name
        (Format.pp_print_list
           ~pp_sep:Format.pp_print_newline
           Format.pp_print_string)
        (List.sort String.compare available_templates)
  | exception Gitlab.Inclusion_not_found {path; include_path} ->
      error
        "%s: Could not find included file '%s'"
        (path_opt_to_string path)
        include_path
  | exception Gitlab.Parse_error {path; message; value; _} ->
      error
        "%s: %s@.@.YAML source: %a"
        (path_opt_to_string path)
        message
        Yaml.pp
        value
