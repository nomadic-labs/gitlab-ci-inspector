(*****************************************************************************)
(*                                                                           *)
(* SPDX-License-Identifier: MIT                                              *)
(* Copyright (c) 2023 Nomadic Labs. <contact@nomadic-labs.com>               *)
(*                                                                           *)
(*****************************************************************************)

(** Represents a parsed settings file. *)
type t = {
  gitlab_token : string option;
  gitlab_project : string option;
  environments : Config_environment.map;
  scalar_style : Yaml.scalar_style;
}

(** Default values for the settings.

    By default,
     - [gitlab_token] is [None]
     - [gitlab_project] is [None]
     - [environments] is empty
     - [scalar_style] is [`Plain] *)
val default : t

(** Finds and load the settings, if any.

    Setting are loaded from, by order of priority:

    - [path_opt] if not [None].
    - The environment variable [GCI_SETTINGS], if set.
    - The [.gci.json] file in the current folder or any parent folders.

    This function then overrides any settings loaded from file above can be
    overridden by the following environments variables:

     - [GCI_GL_TOKEN] overrides the field [gitlab_token]
     - [GCI_GL_PROJECT] overrides the field [gitlab_project]
     - [GCI_ENVIRONMENTS] overrides the field [environments]
     - [GCI_SCALAR_STYLE] overrides the field [scalar_style] *)
val find : Fpath.t option -> t
