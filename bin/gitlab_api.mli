(*****************************************************************************)
(*                                                                           *)
(* SPDX-License-Identifier: MIT                                              *)
(* Copyright (c) 2023 Nomadic Labs. <contact@nomadic-labs.com>               *)
(*                                                                           *)
(*****************************************************************************)

(** Parsed result from a GitLab Lint API call.

    See {{https://docs.gitlab.com/ee/api/lint.html}GitLab CI Lint
    API} for more information.
*)
type linting_response = {
  valid : bool;
  merged_yaml : string option;
  errors : string list;
  warnings : string list;
}

type error =
  | Unauthorized
  | Other of (string * Yojson.Basic.t)
  | Unrecognized_json of Yojson.Basic.t
  | Unknown of Yojson.Basic.t

(** Lints a CI configuration.

    [lint ~project ~content] lints the CI configuration [content]
    using [project]'s lint API. If supplied, [token] is used for
    authentification. *)
val lint :
  ?token:string ->
  project:string ->
  content:string ->
  unit ->
  (linting_response, error) Result.t Lwt.t
